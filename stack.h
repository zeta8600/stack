/*
 * stack.h  by zeta
 *
 * 概要:
 * 構造体の隠蔽と関数ポインタを用いた
 * 安全なオブジェクト指向を目指すつもりだったプログラム．
 * それなりにセキュア(だと思う)
 *
 * stackとは?:
 * データ構造の一つ
 * データをbufferに入れることをpush，取り出すことをpopという
 * 一番新しくpushしたデータが，一番早くpopされる
 * イメージは本を積み上げる
 * 詳しくはgoogle先生に
 *
 * 使い方:
 * 1. #includeでstack.hを読み込む
 * 2. STACK*型変数を定義
 *     ex) STACK* stack;
 * 3. initStackで初期化する
 *     ex) stack = initStack(100, sizeof(int), 50);
 * 4. freeStackでメモリの開放
 *     ex) freeStack(stack);
 *
 * 関数一覧
 * STACK* initStack(
 *     size_t,
 *     size_t,
 *     size_t
 * );
 * 初期化したSTACK変数のアドレスを返す
 * 第1引数:stackデータの個数
 * 第2引数:stackデータ1つあたりのサイズ
 * 第3引数:stack bufferが足りなくなったときの増加量
 * 戻り値:
 *     成功時:確保したメモリのアドレス
 *     失敗時:NULL
 *
 *
 * int freeStack(
 *     STACK*
 * );
 * STACK変数の占めるメモリの開放
 * 第1引数:開放する変数のメモリ
 * 戻り値:
 *     成功時:0
 *     失敗時:-1
 *
 *
 * int push(
 *     STACK*,
 *     const void*
 * );
 * STACK構造体に宣言された関数ポインタpushが指す関数
 * stackにpushする
 * 第1引数:pushするstackのアドレス
 * 第2引数:pushするデータのアドレス
 * 戻り値:
 *     成功時:0
 *     失敗時:-1
 * ex)
 * int n = 5;
 * stack->push(stack, &n);
 *
 *
 * int pop(
 *     STACK*
 * );
 * STACK構造体に宣言された関数ポインタpopが指す関数
 * stackからpopする
 * 第1引数:popするstackのアドレス
 * 戻り値:
 *     成功時:0
 *     失敗時:-1
 * ex)
 * stack->pop(&stack);
 *
 *
 * int top(
 *     STACK*,
 *     void*const
 * );
 * STACK構造体に宣言された関数ポインタtopが指す関数
 * stackの現在の値を渡す
 * 第1引数:stackのアドレス
 * 第2引数:値を入れる変数のアドレス
 * 戻り値:
 *     成功時:0
 *     失敗時:-1
 * ex)
 * int n[1];
 * stack->top(&stack, n);
 *
 *
 * サンプルプログラムとしてstack_main.cを配布
 * 実行方法:
 * Linux, Mac:
 * $ gcc -o stack.out stack.c stack_main.c
 * $ ./stack.out
 *
 * Windows:
 * > gcc -o stack.exe stack.c stack_main.c
 * > stack.exe
 */

#ifndef _STACK_H_20170505_
#define _STACK_H_20170505_

typedef struct STACK{
    struct _stack * const vars;                       //諸変数
    int (* const push)(struct STACK*, const void*);   //stackにpushする
    int (* const pop)(struct STACK*);                 //stackにpopする
    int (* const top)(struct STACK*, void*const); //現在のデータを表示
} STACK;

STACK* initStack(size_t, size_t, size_t);
int freeStack(STACK*);

#endif
