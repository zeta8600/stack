/*
 * stack_main.c  by zeta
 *
 * stackライブラリのサンプルプログラム
 *
 * 実行方法:
 * Linux, Mac:
 * terminalに以下のように入力
 * $ gcc -o stack.out stack.c stack_main.c
 * $ ./stack.out
 *
 * Windows:
 * コマンドプロントに以下のように入力
 * > gcc -o stack.exe stack.c stack_main.c
 * > stack.exe
 *
 */

#include <stdio.h>
#include "stack.h"                              //stack.hの読み込み

int main(int argc, char* argv[])
{
    STACK* stack;                               //STACK*型変数の宣言
    stack = initStack(10, sizeof(int), 5);      //stackの初期化
    if (stack == NULL) return -1;               //初期化失敗

    for (int i=0; i<20; i++) {
        int n;
        printf("push %d to a stack.\n", i);

        stack->push(stack, &i);                 //stakcにiをpush

        /* stackのtopを取得 */
        if (stack->top(stack, &n) == 0)
            printf("top is %d\n", n);           //成功
        else printf("nothing is in stack.\n");  //失敗
    }
    for (int i=0; i<20; i++) {
        int n;

        /* stackのtopを取得 */
        if (stack->top(stack, &n) == 0)
            printf("top is %d\n", n);           //成功
        else printf("nothig is in a stack.\n"); //失敗

        stack->pop(stack);                      //stackからpopする
        printf("pop from a stack.\n");
    }

    freeStack(stack);                           //stackの開放
    return 0;
}
