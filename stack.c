#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "stack.h"

struct _stack{
    int last;       //現在のデータ位置
    size_t size;    //array1つあたりのサイズ
    size_t num;     //arrayの個数
    size_t numInc;  //arrayの増加分
    void *array;    //stackデータ
};

static bool isNullPtr(STACK* stack)
{
    if (stack == NULL) return true;
    if (stack->vars == NULL) return true;
    if (stack->vars->array == NULL) return true;
    return false;
}

static int stackPush(STACK* stack, const void* data)
{
    //有効アドレスの確認
    if (isNullPtr(stack)) {
        freeStack(stack);
        return -1;
    }

    //buffer sizeの拡大
    if (stack->vars->num <= stack->vars->last+1) {
        stack->vars->num += stack->vars->numInc;
        void *newArray = realloc(stack->vars->array,
                            stack->vars->size * stack->vars->num);
        if (newArray == NULL) {
            freeStack(stack);
            return -1;
        }
        stack->vars->array = newArray;
    }

    stack->vars->last++;
    //arrayへの書き込み
    memcpy(stack->vars->array + stack->vars->size*stack->vars->last,
            data, stack->vars->size);

    return 0;
}

static int stackPop(STACK* stack)
{
    //有効アドレスの確認
    if (isNullPtr(stack)) {
        freeStack(stack);
        return -1;
    }
    //stack量の確認
    if (stack->vars->last < 0) return -1;

    stack->vars->last--;

    //buffer sizeの縮小
    if (stack->vars->last < 0) return 0;
    if (stack->vars->last+1 <= stack->vars->num-stack->vars->numInc) {
        stack->vars->num -= stack->vars->numInc;
        void *newArray = realloc(stack->vars->array,
                            stack->vars->size * stack->vars->num);
        if (newArray == NULL) {
            freeStack(stack);
            return -1;
        }
        stack->vars->array = newArray;
    }

    return 0;
}

static int stackTop(STACK* stack, void*const out)
{
    if (isNullPtr(stack)) {
        freeStack(stack);
        return -1;
    }
    if (stack->vars->last < 0) return -1;

    //最上位データのアドレス
    memcpy(
            out,
            stack->vars->array + stack->vars->size*stack->vars->last,
            stack->vars->size
          );
    return 0;
}

STACK* initStack(size_t num, size_t size, size_t numInc)
{
    //STACKメモリの確保
    STACK *stack = (STACK*)malloc(sizeof(STACK));
    if (stack == NULL) return NULL;

    //varsのメモリの確保
    struct _stack *vars = (struct _stack*)malloc(sizeof(struct _stack));
    if (vars == NULL) {
        free(stack);
        return NULL;
    }

    //STACK.vars->arrayメモリの確保
    vars->array = calloc(num, size);
    if (vars->array == NULL) {
        free(vars);
        free(stack);
        return NULL;
    }

    //諸変数の設定
    if (size*num == 0) {
        free(vars->array);
        free(vars);
        free(stack);
        return NULL;
    }
    vars->size = size;
    vars->num = num;
    if (numInc != 0) vars->numInc = numInc;
    else vars->numInc = num;
    vars->last = -1;

    //const関数ポインタへの無理矢理な代入
    STACK srcStack = {vars, stackPush, stackPop, stackTop};
    memcpy(stack, &srcStack, sizeof(STACK));

    return stack;
}

int freeStack(STACK* stack)
{
    if (stack == NULL) return -1;
    if (stack->vars == NULL) {
        free(stack);
        stack = NULL;
        return -1;
    }
    if (stack->vars->array == NULL) {
        free(stack->vars);
        free(stack);
        stack = NULL;
        return -1;
    }
    free(stack->vars->array);
    free(stack->vars);
    free(stack);
    stack = NULL;
    return 0;
}
